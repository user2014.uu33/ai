// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "Perception/AIPerceptionComponent.h"
#include "SC_BTService_TargetActor_Sight.generated.h"

class UAISense;

UCLASS()
class SCG1_API USC_BTService_TargetActor_Sight : public UBTService_BlackboardBase
{
	GENERATED_BODY()
	
	USC_BTService_TargetActor_Sight(const FObjectInitializer& ObjectInitializer);
	
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
	void ReceiveTickAI(UBehaviorTreeComponent& BTComp, AAIController* OwnerController, APawn* ControlledPawn, float DeltaSeconds);
	
public:

	UPROPERTY(EditAnywhere, Category="AIController")
	TSubclassOf<class AAICustomController> AIOwner;
	
	UPROPERTY(EditAnywhere, Category="AIPerception")
	TSubclassOf<UAISense> SetSenseToUse;

	UPROPERTY(EditAnywhere, Category="AIPerception")
	float BoomerangAttackRadius;
	
	UPROPERTY()
	AAICustomController* LocalAIController;

	UPROPERTY()
	int32 NearestPlayer;
};
