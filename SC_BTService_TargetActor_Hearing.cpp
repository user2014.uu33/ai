#include "SC_BTService_TargetActor_Hearing.h"
#include "AIController.h"
#include "AICustomController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "SCG1/CharacterSystem/NPC/Character/SC_BaseNPCCharacter.h"
#include "Perception/AIPerceptionComponent.h"

USC_BTService_TargetActor_Hearing::USC_BTService_TargetActor_Hearing(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bNotifyBecomeRelevant = true;
}

void USC_BTService_TargetActor_Hearing::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::OnBecomeRelevant(OwnerComp, NodeMemory);
	LocalAIController = NewObject<AAICustomController>(AIOwner);
}

void USC_BTService_TargetActor_Hearing::ReceiveTickAI(UBehaviorTreeComponent& BTComp, AAIController* OwnerController,
	const APawn* ControlledPawn, float DeltaSeconds) const
{
	TArray<AActor*> OutActors;
	
	if(ControlledPawn->GetController() == nullptr)
		return;

	ControlledPawn->GetController()->FindComponentByClass<UAIPerceptionComponent>()->GetCurrentlyPerceivedActors(
		SetSenseToUse, OutActors);
	
	if (OutActors.IsEmpty())
	{
		return;
	}

	BTComp.GetBlackboardComponent()->SetValueAsObject(FName("TargetActor"), OutActors[0]);
}

void USC_BTService_TargetActor_Hearing::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	ASC_BaseNPCCharacter* LocalNPCharacter = Cast<ASC_BaseNPCCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	
	if(LocalNPCharacter)
		ReceiveTickAI(OwnerComp, LocalAIController, LocalNPCharacter, DeltaSeconds);
}