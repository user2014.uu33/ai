// Fill out your copyright notice in the Description page of Project Settings.

#include "SC_BaseNPCCharacter.h"

#include "AIController.h"
#include "BrainComponent.h"
#include "NiagaraComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "SCG1/AbilitySystem/AttributeSets/SC_AttributeSet.h"
#include "SCG1/AbilitySystem/Components/SC_AbilitySystemComponent.h"
#include "SCG1/CharacterSystem/NPC/AI/HumanAI/SC_BBHumanEnemyKeys.h"
#include "SCG1/Items/Equippable/SC_Equippable.h"
#include "Components/SphereComponent.h"
#include "SCG1/CharacterSystem/NPC/AI/AICustomController.h"
#include "SCG1/CharacterSystem/Player/Character/SC_BasePlayerCharacter.h"
#include "SCG1/Items/Equippable/Weapon/SC_MeleeWeapon.h"

#include "SCG1/UI/HUD/SC_AttributeBar.h"

// Sets default values
ASC_BaseNPCCharacter::ASC_BaseNPCCharacter()
{
	NiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(FName("Niagara"));
	NiagaraComponent->SetupAttachment(GetMesh(), FName("Spine"));
	
	bUseControllerRotationYaw = true;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	
	HardRefAbilitySystemComponent = CreateDefaultSubobject<USC_AbilitySystemComponent>("AbilitySystemComponent");
	HardRefAbilitySystemComponent->SetIsReplicated(true);

	// Minimal Mode means that no GameplayEffects will replicate. They will only live on the Server. Attributes, GameplayTags, and GameplayCues will still replicate to us.
	HardRefAbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Minimal);
	
	HardRefAttributes = CreateDefaultSubobject<USC_AttributeSet>(TEXT("AttributeSet"));

	AbilitySystemComponent = HardRefAbilitySystemComponent;
	Attributes = HardRefAttributes;
	
	StatusBarsWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("StatusBars");
	StatusBarsWidgetComponent->SetupAttachment(GetMesh());
}

void ASC_BaseNPCCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	InitializeCharacter();
	InitializeAbilitySystemComponent();
}

void ASC_BaseNPCCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ASC_BaseNPCCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(GetLocalRole() != ROLE_Authority)
	{
		//TODO: optimize this if possible. Take into account that camera reference may change any time
		const FVector CameraLocation = GetWorld()->GetFirstPlayerController()->PlayerCameraManager->GetCameraLocation();
		const FVector WidgetLocation = StatusBarsWidgetComponent->GetComponentLocation();
	
		FRotator TargetRotation = UKismetMathLibrary::FindLookAtRotation(WidgetLocation, CameraLocation);
		TargetRotation.Roll = 0;
		TargetRotation.Pitch = 0;

		StatusBarsWidgetComponent->SetWorldRotation(TargetRotation);
	}
}

void ASC_BaseNPCCharacter::SpawnNPCWeapon_Implementation(FName SocketName)
{
	FTransform SpawnTransform;
	
	SpawnTransform.SetTranslation(this->GetActorLocation());
	
	SpawnedMeleeWeapon = GetWorld()->SpawnActorDeferred<ASC_MeleeWeapon>(MeleeWeapon, SpawnTransform,
		this, this->GetInstigator(), ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	SpawnedMeleeWeapon->FinishSpawning(SpawnTransform);

	SpawnedMeleeWeapon->AttachToComponent(this->GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
}

void ASC_BaseNPCCharacter::TakeWeaponToMainHand()
{
	if (SpawnedMeleeWeapon)
	{
		SpawnedMeleeWeapon->AttachToComponent(this->GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, "Melee_MainHand");
	}
}

void ASC_BaseNPCCharacter::HideWeaponFromMainHand()
{
	if (SpawnedMeleeWeapon)
	{
		SpawnedMeleeWeapon->AttachToComponent(this->GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, "Melee_MainHand_Back");
	}
}

void ASC_BaseNPCCharacter::OnStunTagChanged(const FGameplayTag Tag, int32 Count)
{
	Super::OnStunTagChanged(Tag, Count);

	HandleAILogicOnCrowdControlStateChanged(Count);
}

void ASC_BaseNPCCharacter::OnKnockBackTagChanged(const FGameplayTag Tag, int32 Count)
{
	Super::OnKnockBackTagChanged(Tag, Count);

	HandleAILogicOnCrowdControlStateChanged(Count);
}

void ASC_BaseNPCCharacter::OnKnockDownTagChanged(const FGameplayTag Tag, int32 Count)
{
	Super::OnKnockDownTagChanged(Tag, Count);

	HandleAILogicOnCrowdControlStateChanged(Count);
}

// void ASC_BaseNPCCharacter::OnHealthChanged(const FOnAttributeChangeData& Data)
// {
// 	Super::OnHealthChanged(Data);
// 	
// 	if (!IsValid(HealthBar)) return;
// 	
// 	HealthBar->SetCurrentAttributeValue(Data.NewValue);
// }
//
// void ASC_BaseNPCCharacter::OnMaxHealthChanged(const FOnAttributeChangeData& Data)
// {
// 	Super::OnMaxHealthChanged(Data);
//
// 	if (!IsValid(HealthBar)) return;
//
// 	HealthBar->SetMaxAttributeValue(Data.NewValue);
// }

void ASC_BaseNPCCharacter::HandleAILogicOnCrowdControlStateChanged(int Count)
{
	const AAIController* AIController = Cast<AAIController>(GetController());
	if (!IsValid(AIController)) return;

	UBrainComponent* BrainComponent = AIController->GetBrainComponent();
	if (!IsValid(BrainComponent)) return;
	
	if(IsTargetUnderHardControl())
	{
		BrainComponent->GetBlackboardComponent()->SetValueAsBool(SC_BBHumanEnemyKeys::IsHardCC, true);
		//BrainComponent->PauseLogic(TEXT("CC Begin"));
		return;
	}
	
	BrainComponent->GetBlackboardComponent()->SetValueAsBool(SC_BBHumanEnemyKeys::IsHardCC, false);
	//BrainComponent->ResumeLogic(TEXT("CC Finished"));
}

void ASC_BaseNPCCharacter::OnDeathEnd_Implementation()
{
}

void ASC_BaseNPCCharacter::StartDeath()
{
	Super::StartDeath();
	// const float DeathMontageDuration = GetMesh()->GetAnimInstance()->Montage_Play(DeathMontage);
	// FTimerHandle TimerHandle;
	// GetWorldTimerManager().SetTimer(TimerHandle, this, &ASC_BaseNPCCharacter::EndDeath, DeathMontageDuration);

	if (HasAuthority())
	{
		if(const AAIController* AIController = Cast<AAIController>(GetController()))
		{
			if (AIController->BrainComponent)
			{
				AIController->BrainComponent->StopLogic("Death");
			}
			DetachFromControllerPendingDestroy();
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("NPC - Start death"));

	EnableRagdoll();
}

void ASC_BaseNPCCharacter::EndDeath()
{
	OnDeathEnd();
	if (OnDeath.IsBound()) OnDeath.Broadcast();

	if (HasAuthority())
	{
		FTimerHandle TimerHandle;
		FTimerDelegate TimerCallback;
		TimerCallback.BindLambda([&]() { Destroy(); });
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerCallback, 0.1f, false);
	}
}

void ASC_BaseNPCCharacter::InitializeAbilitySystemComponent()
{
	AbilitySystemComponent = HardRefAbilitySystemComponent;
	AbilitySystemComponent->InitAbilityActorInfo(this, this);

	Super::InitializeAbilitySystemComponent();
}

void ASC_BaseNPCCharacter::InitializeAttributeSet()
{
	Super::InitializeAttributeSet();

	Attributes = HardRefAttributes;
}

void ASC_BaseNPCCharacter::StartTimerToResetAbility()
{
	FTimerHandle TimerHandle;
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &ASC_BaseNPCCharacter::ResetAbility, TimeToHandAttack);
}

void ASC_BaseNPCCharacter::ResetAbility()
{
	AAICustomController* LocalController = Cast<AAICustomController>(GetController());

	if(LocalController)
		LocalController->GetBlackboardComponent()->SetValueAsBool(FName("ResetMeleeAttack"), false);
}