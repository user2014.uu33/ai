// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_BTService_TargetActor_Sight.h"
#include "AIController.h"
#include "AICustomController.h"
#include "VectorTypes.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "SCG1/CharacterSystem/NPC/Character/SC_BaseNPCCharacter.h"
#include "Perception/AIPerceptionComponent.h"

USC_BTService_TargetActor_Sight::USC_BTService_TargetActor_Sight(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bNotifyBecomeRelevant = true;
}

void USC_BTService_TargetActor_Sight::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::OnBecomeRelevant(OwnerComp, NodeMemory);
	LocalAIController = NewObject<AAICustomController>(AIOwner);
}

void USC_BTService_TargetActor_Sight::ReceiveTickAI(UBehaviorTreeComponent& BTComp, AAIController* OwnerController,
	 APawn* ControlledPawn, float DeltaSeconds)
{
	TArray<AActor*> OutActors;

	if(ControlledPawn->GetController() == nullptr)
		return;
	
	ControlledPawn->GetController()->FindComponentByClass<UAIPerceptionComponent>()->GetCurrentlyPerceivedActors(
		SetSenseToUse, OutActors);
	
	if (OutActors.IsEmpty())
	{
		BTComp.GetBlackboardComponent()->ClearValue(FName("TargetActor"));

		ASC_BaseNPCCharacter* LocalNPCharacter = Cast<ASC_BaseNPCCharacter>(ControlledPawn);
		LocalNPCharacter->GetCharacterMovement()->MaxWalkSpeed = LocalNPCharacter->NPCDefaultSpeed;
		
		return;
	}

	FVector NPCPosition = ControlledPawn->GetActorLocation();
	TArray<float> LenghtToActor;
	
	for(auto It : OutActors)
		LenghtToActor.Add(FVector(NPCPosition - It->GetActorLocation()).Length());

	LenghtToActor.Sort();

	for(int32 i = 0; i < OutActors.Num(); ++i)
	{
		if(static_cast<int>(FVector(NPCPosition - OutActors[i]->GetActorLocation()).Length()) == static_cast<int>(LenghtToActor[0]))
		{
			BTComp.GetBlackboardComponent()->SetValueAsObject(FName("TargetActor"), OutActors[i]);

			ASC_BaseNPCCharacter* LocalNPCharacter = Cast<ASC_BaseNPCCharacter>(ControlledPawn);
			
			LocalNPCharacter->GetCharacterMovement()->MaxWalkSpeed = LocalNPCharacter->NPCTargetSpeed;
			break;
		}
	}
}

void USC_BTService_TargetActor_Sight::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	ASC_BaseNPCCharacter* LocalNPCharacter = Cast<ASC_BaseNPCCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	
	if(LocalNPCharacter)
		ReceiveTickAI(OwnerComp, LocalAIController, LocalNPCharacter, DeltaSeconds);
}