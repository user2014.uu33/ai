#include "AICustomController.h"

#include "BehaviorTree/BehaviorTree.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"
#include "Navigation/CrowdFollowingComponent.h"

#include "SCG1/CharacterSystem/NPC/Character/SC_BaseNPCCharacter.h"

AAICustomController::AAICustomController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>(TEXT("PathFollowingComponent")))
{
	Sense_Sight = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sense_Sight"));
	Sense_Hearing = CreateDefaultSubobject<UAISenseConfig_Hearing>(TEXT("Sense_Hearing"));
	
	AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerceptionComponent"));

	Sense_Sight->SightRadius = 3000;
	Sense_Sight->LoseSightRadius = 3500;
	Sense_Sight->PeripheralVisionAngleDegrees = 180;
	Sense_Sight->DetectionByAffiliation.bDetectEnemies = true;
	Sense_Sight->DetectionByAffiliation.bDetectFriendlies = true;
	Sense_Sight->DetectionByAffiliation.bDetectNeutrals = true;
	Sense_Sight->AutoSuccessRangeFromLastSeenLocation = 1500;

	Sense_Hearing->HearingRange = 1000;
	
	AIPerceptionComponent->ConfigureSense(*Sense_Sight);
	AIPerceptionComponent->ConfigureSense(*Sense_Hearing);

	AIPerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AAICustomController::OnPlayerDetected);
}

void AAICustomController::BeginPlay()
{
	Super::BeginPlay();
}

void AAICustomController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	
	ControlledCharacter = Cast<ASC_BaseNPCCharacter>(InPawn);
	
	if(ControlledCharacter)
		RunBehaviorTree(BehaviorTree);
}

void AAICustomController::OnPlayerDetected(const TArray<AActor*>&DetectedPawn)
{
	IfCanSeeActors(DetectedPawn);
}

TArray<AActor*> AAICustomController::DetectNearestNPC() const
{
	TArray<FOverlapResult> OverlapResults;
	
	GetWorld()->OverlapMultiByChannel(OverlapResults, ControlledCharacter->GetActorLocation(),
		ControlledCharacter->GetActorQuat(), ECC_GameTraceChannel2, FCollisionShape::MakeSphere(Radius));

	TArray<AActor*> LocalActors;
    
	for (FOverlapResult Overlap : OverlapResults)
	{
		AActor* OverlapActor = Overlap.GetActor();
        
		if (OverlapActor != nullptr && OverlapActor->GetName() != ControlledCharacter->GetName() && Cast<ASC_BaseNPCCharacter>(OverlapActor))
			LocalActors.Add(OverlapActor);
	}
	
	return LocalActors;
}

void AAICustomController::IfCanSeeActors(const TArray<AActor*>&DetectedPawn)
{
	if (!HasBeenDetectedActors.IsEmpty())
		return;

	for (auto It : DetectedPawn)
		HasBeenDetectedActors.Add(It);

	FHitResult HitResult;
	FCollisionQueryParams CollisionQueryParams;
	TArray<AActor*> TempActors = DetectNearestNPC();
	TArray<AActor*> NonVisibleActors;
	FVector VectorStart = ControlledCharacter->GetActorLocation();

	CollisionQueryParams.AddIgnoredActor(ControlledCharacter);
	
	for(auto It : TempActors)
	{
		GetWorld()->LineTraceSingleByChannel(HitResult, VectorStart, It->GetActorLocation(),
			ECollisionChannel::ECC_Pawn, CollisionQueryParams);
			
			if(HitResult.GetHitObjectHandle().GetName() == It->GetName())
				ActorsInsideSendingZone.Add(HitResult.GetActor());
			else
				NonVisibleActors.Add(It);
	}
	
	if(!NonVisibleActors.IsEmpty())
	{
		for(auto It : NonVisibleActors)
		{
			if((ControlledCharacter->GetActorLocation() - It->GetActorLocation()).Length() < Sense_Hearing->HearingRange)
			{
				ASC_BaseNPCCharacter* LocalNPC = Cast<ASC_BaseNPCCharacter>(It);
				//LocalNPC->ProbableDetecting();
			}
		}
	}
	
	if(!ActorsInsideSendingZone.IsEmpty())
	{
		for(int32 i = 0; i < ActorsInsideSendingZone.Num(); ++i)
		{
			if(ControlledCharacter->GetClass() == ActorsInsideSendingZone[i]->GetClass())
			{
				FRotator Rotation = CalculateRotAngle(ActorsInsideSendingZone[i], HasBeenDetectedActors[0]);
				
				if(!GetBlackboardComponent()->GetValueAsObject(FName("TargetActor")))
					ActorsInsideSendingZone[i]->SetActorRotation(FRotator(0, Rotation.Yaw, 0));
			}
		}
	}
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AAICustomController::ClearTargetsArray, 5, false);
}

void AAICustomController::ClearTargetsArray()
{
	HasBeenDetectedActors.Empty();
}

FRotator AAICustomController::CalculateRotAngle(const AActor* From, const AActor* To) const
{
	const FVector AICharacter = From->GetActorForwardVector();
	
	const FRotator FindRes = UKismetMathLibrary::FindLookAtRotation(From->GetActorLocation(),
		To->GetActorLocation());

	return FindRes;
}