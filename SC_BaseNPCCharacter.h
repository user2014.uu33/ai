// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SCG1/CharacterSystem/SC_BaseCharacter.h"
#include "SC_BaseNPCCharacter.generated.h"

class USC_AttributeBar;
struct FOnAttributeChangeData;
class UWidgetComponent;
class ASC_MeleeWeapon;
class AAICustomController;

UCLASS(Abstract)
class SCG1_API ASC_BaseNPCCharacter : public ASC_BaseCharacter
{
	
	GENERATED_BODY()

public:
	
	ASC_BaseNPCCharacter();
	virtual void PossessedBy(AController* NewController) override;

protected:
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

#pragma region Ability System

	virtual void OnStunTagChanged(const FGameplayTag Tag, int32 Count) override;
	virtual void OnKnockBackTagChanged(const FGameplayTag Tag, int32 Count) override;
	virtual void OnKnockDownTagChanged(const FGameplayTag Tag, int32 Count) override;

protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USC_AbilitySystemComponent* HardRefAbilitySystemComponent;

	UPROPERTY()
	USC_AttributeSet* HardRefAttributes;
	
	//virtual void OnHealthChanged(const struct FOnAttributeChangeData& Data) override;
	//virtual void OnMaxHealthChanged(const struct FOnAttributeChangeData& Data) override;

#pragma endregion 
	
private:

	/** For AI **/
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess=true))
	class UNiagaraComponent* NiagaraComponent {};
	
	void HandleAILogicOnCrowdControlStateChanged(int Count);

public:
	
	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* DeathMontage;

	FCharacterDeathDelegate OnDeath;
	
	UFUNCTION(BlueprintNativeEvent)
	void OnDeathEnd();

private:
	
	virtual void StartDeath() override;
	virtual void EndDeath();
	
#pragma region Status Bars

	UPROPERTY(EditDefaultsOnly)
	UWidgetComponent* StatusBarsWidgetComponent;

	UPROPERTY()
	USC_AttributeBar* HealthBar;

#pragma endregion

	virtual void InitializeAbilitySystemComponent() override;
	virtual void InitializeAttributeSet() override;

#pragma region NPC Attack
	
public:

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASC_MeleeWeapon> MeleeWeapon;

	UPROPERTY()
	ASC_MeleeWeapon* SpawnedMeleeWeapon;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SpawnNPCWeapon(FName SocketName);

	UFUNCTION()
	void TakeWeaponToMainHand();

	UFUNCTION()
	void HideWeaponFromMainHand();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float NPCTargetSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float NPCDefaultSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	float TimeToHandAttack;

	UFUNCTION()
	void ResetAbility();

	UFUNCTION()
	void StartTimerToResetAbility();

#pragma endregion
};
