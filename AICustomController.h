#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AICustomController.generated.h"

class UAISenseConfig_Sight;
class UAISenseConfig_Hearing;
class UAIPerceptionComponent;

UCLASS()
class SCG1_API AAICustomController : public AAIController
{
	GENERATED_BODY()
	
	AAICustomController(const FObjectInitializer& ObjectInitializer);
	
	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;
	
public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBehaviorTree* BehaviorTree;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="PerceptionSystem")
	UAIPerceptionComponent* AIPerceptionComponent;
	
	UPROPERTY()
	UAISenseConfig_Sight* Sense_Sight;
	
	UPROPERTY()
	UAISenseConfig_Hearing* Sense_Hearing;

	UPROPERTY()
	class ASC_BaseNPCCharacter* ControlledCharacter;

	UPROPERTY()
	TArray<AActor*> ActorsInsideSendingZone;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AreaToSendInfo")
	float Radius;

	UFUNCTION()
	void OnPlayerDetected(const TArray<AActor*>&DetectedPawn);

	UFUNCTION()
	TArray<AActor*> DetectNearestNPC() const;

	UFUNCTION()
	void IfCanSeeActors(const TArray<AActor*>&DetectedPawn);
	
	UFUNCTION()
	FRotator CalculateRotAngle(const AActor* From, const AActor* To) const;

	UPROPERTY()
	TArray<AActor*> HasBeenDetectedActors;

	UFUNCTION()
	void ClearTargetsArray();

	FTimerHandle TimerHandle;
};
